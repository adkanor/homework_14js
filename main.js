let input;
let header = document.querySelector(".header");
function createInput() {
  input = document.createElement("input");
  input.value = "Change theme";
  input.type = "button";
  input.innerHTML = "Change the theme";
  input.style.padding = "30px";
  input.style.marginLeft = "790px";
  input.style.borderRadius = "4px";
  input.style.cursor = "pointer";
  input.style.marginLeft = "750px";
  return input;
}
header.append(createInput());

window.onload = function () {
  if (localStorage.getItem("theme") === "dark") {
    document.body.setAttribute("theme", "dark");
  }
};
input.addEventListener("click", changeTheme);

function changeTheme() {
  if (document.body.hasAttribute("theme")) {
    document.body.removeAttribute("theme");
    localStorage.removeItem("theme");
  } else {
    localStorage.setItem("theme", "dark");
    document.body.setAttribute("theme", "dark");
  }
}
